package com.bakaeva.tm.endpoint;

import com.bakaeva.tm.api.IServiceLocator;
import com.bakaeva.tm.entity.Session;
import com.bakaeva.tm.entity.User;
import com.bakaeva.tm.enumerated.Role;
import com.bakaeva.tm.exception.empty.EmptyLoginException;
import com.bakaeva.tm.exception.security.AccessDeniedException;
import com.sun.istack.internal.Nullable;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@NoArgsConstructor
@AllArgsConstructor
public final class SessionEndpoint {

    @NotNull
    private IServiceLocator serviceLocator;

    @WebMethod
    public void closeSession(@WebParam(name = "session") @Nullable Session session) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getSessionService().close(session);
    }

    @WebMethod
    public void closeSessionAll(@WebParam(name = "session") @Nullable Session session) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getSessionService().closeAll(session);
    }

    @Nullable
    @WebMethod
    public User getUserBySession(@WebParam(name = "session") @Nullable Session session) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getSessionService().getUser(session);
    }

    @NotNull
    @WebMethod
    public String getUserIdBySession(@WebParam(name = "session") @Nullable Session session) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getSessionService().getUserId(session);
    }

    @NotNull
    @WebMethod
    public List<Session> findSessionAll(@WebParam(name = "session") @Nullable Session session) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getSessionService().findAll(session);
    }

    @Nullable
    @WebMethod
    public Session openSession(
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "password") @Nullable String password
    ) {
        if (login == null) throw new EmptyLoginException();
        if (password == null) throw new EmptyLoginException();
        return serviceLocator.getSessionService().open(login, password);
    }

    @WebMethod
    public void signOutByLogin(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "login") @Nullable String login
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getSessionService().signOutByLogin(login);
    }

    @WebMethod
    public void signOutByUserId(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "userId") @Nullable String userId
    ) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getSessionService().signOutByUserId(userId);
    }

    @WebMethod
    public void removeSessionAll(@WebParam(name = "session") @Nullable Session session) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getSessionService().clear();
    }

    @Nullable
    @WebMethod
    public Session removeSession(@WebParam(name = "session") @Nullable Session session) {
        if (session == null) throw new AccessDeniedException();
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getSessionService().remove(session);
    }

}