package com.bakaeva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import com.bakaeva.tm.enumerated.Role;

public interface IAuthService {

    @NotNull
    String getUserId();

    void login(@Nullable String login, @Nullable String password);

    void checkRoles(@Nullable Role[] roles);

    boolean isAuth();

    void logout();

    void registry(@Nullable String login, @Nullable String password, @Nullable String email);

}