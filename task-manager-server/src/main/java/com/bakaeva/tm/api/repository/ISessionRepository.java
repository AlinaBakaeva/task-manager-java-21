package com.bakaeva.tm.api.repository;

import com.bakaeva.tm.api.IRepository;
import com.bakaeva.tm.entity.Session;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    List<Session> findByUserId(@NotNull String userId);

    void removeByUserId(@NotNull String userId);

    @Nullable
    Session findById(@NotNull final String id);

}