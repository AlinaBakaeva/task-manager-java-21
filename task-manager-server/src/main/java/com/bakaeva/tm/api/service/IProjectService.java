package com.bakaeva.tm.api.service;

import com.bakaeva.tm.api.IService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import com.bakaeva.tm.entity.Project;

import java.util.List;

public interface IProjectService  extends IService<Project> {

    void create(@Nullable String userId, @Nullable String name);

    void create(@Nullable String userId, @Nullable String name, @Nullable String description);

    void add(@Nullable String userId, @Nullable Project project);

    void remove(@Nullable String userId, @Nullable Project project);

    void clear(@Nullable String userId);

    @NotNull
    List<Project> findAll(@Nullable String userId);

    @Nullable
    Project findById(@Nullable String userId, @Nullable String id);

    @Nullable
    Project findByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    Project findByName(@Nullable String userId, @Nullable String name);

    @Nullable
    Project removeById(@Nullable String userId, @Nullable String id);

    @Nullable
    Project removeByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    Project removeByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Project updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    Project updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

}