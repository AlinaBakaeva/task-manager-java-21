package com.bakaeva.tm.command;

import com.bakaeva.tm.api.IEndpointLocator;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractCommand {

    protected IEndpointLocator endpointLocator;

    public abstract String name();

    public abstract String argument();

    public abstract String description();

    public abstract void execute();

}